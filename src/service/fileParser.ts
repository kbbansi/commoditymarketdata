// file parsing happens here

import fs  from "fs";
import { handleEvent } from "../handlers/file.event";




export const parseAndStoreData = (csvFilePath: string) => {
    const fileStream = fs.createReadStream(csvFilePath, {encoding: 'utf8'});
    handleEvent(fileStream)
}

module.exports = {
    parseAndStoreData
}