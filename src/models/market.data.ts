import mongoose from "mongoose";

export interface IMarketData {
    Commodity: string;
    Market: string;
    Price: number;
    Date: Date;
}

interface MarketDataDoc extends mongoose.Document{
    Commodity: string;
    Market: string;
    Price: number;
    Date: Date;
}


interface MarketDataModelInterface extends mongoose.Model<MarketDataDoc> {
    build(data: IMarketData): MarketDataDoc
}

// mongo schema def
const marketDataSchema =  new mongoose.Schema({
    Commodity: {
        type: String,
        required: true
    },

    Market: {
        type: String,
        required: true
    },

    Price: {
        type: Number,
        required: true
    },

    Date: {
        type: Date,
        required: true
    }
}, {collection: 'marketData'});


marketDataSchema.statics.build = (data: IMarketData) => {
    return new MarketData(data)
}

const MarketData = mongoose.model<MarketDataDoc, MarketDataModelInterface>('MarketData', marketDataSchema)



export {MarketData}