import { db } from "./config/mongo.config";
import { parseAndStoreData } from "./service/fileParser";

console.log('Hello File Parser');

const fileName = "./src/sample_market_price_data.csv";

const  start = async () => {
    try {
        await db()
        parseAndStoreData(fileName)
    } catch (error) {
        console.error(error)
    }
}

start()