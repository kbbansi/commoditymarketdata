import mongoose from "mongoose";
import config from "dotenv";

config.config()
const connectionParam = {
    host: process.env.MONGO_URL || 'mongodb://',
    user: process.env.MONGO_USER || 'kwabena_market',
    password: process.env.MONGO_PASSWORD || 'kwabea_13',
    database: process.env.MONGO_DB || 'market_data',
    port: process.env.MONGO_PORT || '27017',
    address: '127.0.0.1'
}

const db = async () => {
    const connectionString = `${connectionParam.host}${connectionParam.user}:${connectionParam.password}@${connectionParam.address}:${connectionParam.port}/${connectionParam.database}`;
    try {
        const connectionStatus = await mongoose.connect(connectionString)
        return connectionStatus
    } catch (err:any) {
        console.log(err.message)
        throw new Error(err.message)
    }
}

export { db }