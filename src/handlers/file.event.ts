import fs from "fs";
import csv from "csv-parser";
import { MarketData, IMarketData } from "../models/market.data";


export const handleEvent = (event: fs.ReadStream)  => {
    let hasData: boolean = false;
    const marketData: IMarketData[] = []
    const csvOptions = {
        headers: ['Commodity', 'Market', 'Price', 'Date'],
        skipLines: 1
    }

    event.pipe(csv(csvOptions)).on('data', (data: IMarketData) => {
        hasData = true;
        // we can add an intermediate step here for further processing
        marketData.push(data)
    });

    event.on('end', async () => {
        if (!hasData) {
            console.log("File has no data!")
        } else {
            console.log("File reading complete")
            console.log(`Read: ${marketData.length} lines`)
            // store data to mongo db
            try {
             await storeData(marketData)
             console.log("Data stored successfully")
            } catch (error:any) {
                console.error("Error storing data: ", error.message)
                console.log("Resuming/retrying data storage...")
                await resumeDataStorage(marketData);
            }
        }
    });

    event.on('error', (err: NodeJS.ErrnoException) => {
        if (err.code === "ENOENT") {
            console.log(err)
        }
    });
}

const storeData = async (data: IMarketData[]) => await Promise.all(data.map(market => MarketData.build(market).save()));

const resumeDataStorage = async (data: IMarketData[]) => {
    for (const market of data) {
        try {
            await MarketData.build(market).save();
            console.log("Data stored successfully")
        } catch (error) {
            console.error("Error storing data:", error);
            console.log("Skipping entry...")
        }
    }
}